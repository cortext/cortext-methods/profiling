# Profiling

Profiling is a method in
[CorTexT Manager](https://docs.cortext.net/) that offers very similar analytical capacities than  [contingency analysis](https://docs.cortext.net/contingency-matrix/).  Given two fields of interest, it will consider a target entity in the first field and produce a visualization of how biased the entities of the second field are distributed in documents which have been tagged by this target entity.
For instance, one may be interested to visualize the full profile of one source with respect to the year of publication, words being used in its documents or any kind of  metadata attached to individual documents.

For more usage details, see the method's
[user documentation](https://docs.cortext.net/profiling/).

## Local deployment

Initially, it is essential to perform the following steps on your local machine:

- Create a symbolic link for [cortextlib/src/cortextlib](https://gitlab.com/cortext/cortext-methods/cortextlib/-/tree/master/src/cortextlib?ref_type=heads) inside `cortext-env/cortext/cortext-methods-transition/profiling` 
- Include (if it does not exist already) the current script reference to the table of scripts (see file [table-script-datas.sql](https://gitlab.com/cortext/cortext-manager/-/blob/dev/data/table-script-datas.sql?ref_type=heads)).
- Inside the vagrant run `sudo mysql -u root ct_manager < /srv/cortext/cortext-manager/data/table-script-datas.sql` and `sudo mysql -u root ct_manager < /srv/setup/config_files/cortext/cortext-methods/table-script-datas.sql` to take into account the changes you just did to the scripts table.
- Build the docker image inside vagrant in `cortext-methods/profiling` : `docker build -t cortext-methods/profiling .`

## License

Copyright (C) 2024 CorTexT

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.