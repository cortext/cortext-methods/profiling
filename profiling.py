#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cortextlib.legacy import containerize

import os, sys
try:
	reload(sys) 
except:
	pass
try:
	sys.setdefaultencoding("utf-8")
except:
	pass
from sqlite3 import *
import types
#sys.path.append("library")
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm

plt.ioff()
#import network_layout
import networkx as nx
import numpy as np
import shutil
import re
from librarypy.path import *
from librarypy import fonctions
#import layout
import copy
#import percolation
from copy import deepcopy
import pickle
try:
	import simplejson as json 
except:
	import json
from operator import itemgetter
import time
import random
import logging,pprint
import pandas as pd
#import bokeh
from matplotlib import pyplot as plt
import numpy as np
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import math
from matplotlib.ticker import NullFormatter  # useful for `logit` scale
from matplotlib.ticker import ScalarFormatter
from matplotlib.ticker import LogLocator, LogFormatterSciNotation
import seaborn as sns

import statsmodels as sm
import statsmodels.stats
import statsmodels.stats.proportion


starttime0=time.time()

#############################################################################
#############################################################################
#################################-FONCTIONS-#################################
#############################################################################
#############################################################################
sign = lambda a: (a>0) - (a<0)

def get_position(field1):
	field1_locx = field1 + '_' + 'pos_x'
	field1_locy = field1 + '_' + 'pos_y'
	data_locx1 = fonctions.get_results(field1_locx,curs)
	data_locy1 = fonctions.get_results(field1_locy,curs)
	data_ent = fonctions.get_results(field1,curs)
	pos1 = {}
	maxx,minx,maxy,miny=-1000000,100000,-1000000,100000
	for id,val in data_ent.iteritems():
		ii=-1
		for va in val:
			ii+=1
			if not va in pos1:
				maxx=max(float(data_locx1[id][ii]),maxx)
				minx=min(float(data_locx1[id][ii]),minx)
				maxy=max(float(data_locy1[id][ii]),maxy)
				miny=min(float(data_locy1[id][ii]),miny)
				
				pos1[va] = (data_locx1[id][ii],data_locy1[id][ii])
	print ('maxx,minx,maxy,miny',maxx,minx,maxy,miny)
	for va in pos1:
		pos1[va] = (str((float(pos1[va][0])-minx)/(maxx-minx)),str((float(pos1[va][1])-miny)/(maxy-miny)))
	return pos1

def provide_attribute(G,poids_y,list_item2_y,list_item1_y,high=False):
	for x in G.nodes():
		G.node[x]['weight']=poids_y.get(x,0.02)
		if x in list_item1_y and not x in list_item2_y :
			G.node[x]['shape']='o'
		if x in list_item2_y and not x in list_item1_y :
			G.node[x]['shape']='^'
		if x in list_item1_y and x in list_item2_y:
			G.node[x]['shape']='v'#so^>v<dph8
		if x in list_item1_y and not x in list_item2_y :
			G.node[x]['category']=field1
		if x in list_item2_y and not x in list_item1_y :
			G.node[x]['category']=field2
		if x in list_item1_y and x in list_item2_y :
			G.node[x]['category']=field1+' & '+field2#so^>v<dph8
		if high:
			G.node[x]['category']=field1+' & '+field2
			G.node[x]['level']='high'
		else:
			G.node[x]['level']='low'
		#print 'ici',G.node[x]
	return G



def convert2list(distribution):
	convlist=[]
	for x,mult in distribution.iteritems():
		for i in range(mult):
			convlist.append(x)
	return convlist


def crosscount(nodes,links,positions):
	# convert the number list into a dictionary of nodes:(x,y)
	posdict=dict([(nodes[i],positions[nodes[i]]) for i in range(0,len(nodes))])
	total=0
	indices=[]
	# loop through every pair of links
	for i in range(len(links)):
		for j in range(i+1,len(links)):
			if len(set(list(links[i])) & set(list(links[j])))>0:#même source ou même destination de liens
				pass
			else:
				# get the locations 
				(x1,y1),(x2,y2)=posdict[links[i][0]],posdict[links[i][1]]
				(x3,y3),(x4,y4)=posdict[links[j][0]],posdict[links[j][1]]
				den=(y4-y3)*(x2-x1)-(x4-x3)*(y2-y1)

				# den==0 if the lines are parallel
				if den==0: continue

				# otherwise ua and ub are the fraction of the line where they cross
				ua=((x4-x3)*(y1-y3)-(y4-y3)*(x1-x3))/den
				ub=((x2-x1)*(y1-y3)-(y2-y1)*(x1-x3))/den

				# if the fraction is between 0 and 1 for both lines then they cross each other
				if (ua>0 and ua<1 and ub>0 and ub<1): 
					total+=1
					indices.append((i,j))		
	return(total,indices)


	
def get_biparti(y,robustness=False):
	print ('building bipartite matrix: period',y)
	democratic=parameters_user.get('democratic',False)		
	biparti={}
	results2_top = results2_y_top[y]
	results1_top = results1_y_top[y]
	#print 'results1_top',results1_top
	#if weight_table:
	biparti1={}
	biparti2={}		
	print ("parameters_user.get('localcooc',False)",parameters_user.get('localcooc',False)		)
	print ('localcoocrange',int(parameters_user.get('localcoocrange',5)))
	print ('twin dans get_bip',twin)
	#print 'results1_top',results1_top
	#print "results2_top",results2_top
	for i,x in enumerate(results1_top):
		#print i,x
		if x in results2_top:
			#print 'this is homicide ' ,x," results1_top[x],results2_top[x]",results1_top[x],results2_top[x]
			biparti = fonctions.add_links_clique(biparti,results1_top[x],results2_top[x],type_biparti,rank=rank,weight_table=weight_dict.get(x,1),local_cooc=parameters_user.get('localcooc',False),localcoocrange=int(parameters_user.get('localcoocrange',5)),democratic=democratic,twin=twin,contextdecay=parameters_user.get('contextdecay',''))
			if robustness:
				if i%2:
					biparti1=fonctions.add_links_clique(biparti1,results1_top[x],results2_top[x],type_biparti,rank=rank,weight_table=weight_dict.get(x,1),local_cooc=parameters_user.get('localcooc',False),localcoocrange=int(parameters_user.get('localcoocrange',5)),democratic=democratic,twin=twin,contextdecay=parameters_user.get('contextdecay',''))
				else:
					biparti2=fonctions.add_links_clique(biparti2,results1_top[x],results2_top[x],type_biparti,rank=rank,weight_table=weight_dict.get(x,1),local_cooc=parameters_user.get('localcooc',False),localcoocrange=int(parameters_user.get('localcoocrange',5)),democratic=democratic,twin=twin,contextdecay=parameters_user.get('contextdecay',''))
	#biparti_y[y] = biparti
	print ('built bipartite matrix: period',y)
	#print 'biparti',biparti
	return biparti,biparti1,biparti2

def get_date(item,tablesource,items_dates):
	if tablesource=='ISICitedRef' or 'Cited_Ref'in tablesource:
		try:
			return int(item.split('_')[1])
		except:
			return 2000
	else:
		
		dates_sorted = items_dates[item]
		dates_sorted.sort()
		ii = quantile*len(dates_sorted)
		#print "item,dates_sorted[int(ii)],dates_sorted",item,dates_sorted[int(ii)],dates_sorted
		return dates_sorted[int(ii)]


#############################################################################
#############################################################################
##########################-LOADING PARAMETERS-###############################
#############################################################################
#############################################################################
try:
	print ('user_parameters',user_parameters)
except:
	user_parameters=''

print ('user_parameters',user_parameters)
parameters_user=fonctions.load_parameters(user_parameters)
print (parameters_user)
timing = parameters_user.get('periods','ISIpubdate')
if timing == 'Custom Periods':
	time_table = 'ISIpubdate_custom'
else:
	time_table = 'ISIpubdate'
corpus_file = parameters_user.get('corpus_file','')
result_path=parameters_user.get('result_path','')
result_path0=result_path[:]
fonctions.progress(result_path0,0)

only_previous_generation=parameters_user.get('only_previous_generation',True)
field1=parameters_user.get('field1','')
field2=parameters_user.get('field2','')
print ('field1field1',field1)
if field2=='':
	field2=field1
field1free=parameters_user.get('field1free','')
field2free=parameters_user.get('field2free','')
if len(field1free)>0:
	field1=field1free
if len(field2free)>0:
	field2=field2free

historical=parameters_user.get('historical',False)
 

try:
	nb_period=int(parameters_user.get('nb_period',1))
except:
	try:
		nb_period=list(parameters_user.get('nb_period',1))
	except:
		nb_period_range = parameters_user.get('nb_period','')
		eval('nb_period = ' + nb_period_range)
time_cut_type=parameters_user.get('time_cut_type','homogeneous')
print ('time_cut_type',time_cut_type)
top=int(parameters_user.get('top','50'))
print ('top here it is:',top)
if not 'Users/jean-philippecointet/' in os.getcwd() and not  'jpcointet/Desktop/cortext' in os.getcwd():
	if not str(top)[-1]=='9':
		top=min(top,500)
	
type_biparti=parameters_user.get('type_biparti',False)
twin=False
if field1==field2:
	type_biparti=True
	twin = True
	
distance_mode = parameters_user.get('distance_mode','cooc')
print ('distance_mode',distance_mode)
distance_type=parameters_user.get('distance_type','chi2')
intertemporal_threshold=float(parameters_user.get('intertemporal_threshold',0.25))
size_community_threshold=parameters_user.get('size_community_threshold',1)
try:
	size_community_threshold=int(size_community_threshold)
except:
	pass
size_community_threshold=max(size_community_threshold,1)
		
projection=parameters_user.get('compute_projection',True)
corpus_type=parameters_user.get('corpus_type','isi')
print ('type_biparti param loading',type_biparti)

try:
	print ('here we are')
	from librarypy import descriptor
	data=descriptor.get_descriptor(corpus_file)
	#print 'data',data
	corpus_type=data['corpus_type']
except:
	corpus_type='other'
no_orphan=parameters_user.get('no_orphan',False)
font=parameters_user.get('font',False)
optimal_comm_nb=parameters_user.get('optimal_comm_nb',False)
community_detection_method=parameters_user.get('community_detection_method','louvain')
print (distance_type,'distance_type')
distance_thres= parameters_user.get('distance_thres',0.)
if distance_thres=='':
	distance_thres=0.


try:
	field1_locx = field1 + '_' + 'pos_x'
	field2_locy = field2 + '_' + 'pos_y'
	fonctions.afficher_dict(get_results(field1_locx),10)
	fonctions.afficher_dict(get_results(field1),10)
except:
	pass

	
distance_thres=float(distance_thres)
norm= parameters_user.get('normalisationtype','norm')
k_nearest=int(parameters_user.get('k_nearest','99999'))
top_edges=int(parameters_user.get('top_edges','999999'))
type_export=parameters_user.get('type_export','json')

#contingency_analysis=parameters_user.get('contingencyanalysis',True)
contingency_analysis=True
print ('contingency_analysis',contingency_analysis)
computeoptimaldistancethres=parameters_user.get('computeoptimaldistancethres',False)
computeoptimaldistancethres=False
compute_global_optimal_distance_thres=parameters_user.get('compute_global_optimal_distance_thres',True)
compute_global_optimal_distance_thres=False
normalize_distribution=parameters_user.get('normalize_distribution',False)
print ('normalize_distribution',normalize_distribution)
if contingency_analysis:
	if parameters_user.get('normalize_distribution',False)==True:
		distance_type="raw"
		distance_type="chi2_dir"
	elif parameters_user.get('normalize_distribution',False)==False:
		print ('using chi2_dir')
		distance_type="chi2_dir"
	else:
		distance_type="cooc_deviation"
	if parameters_user.get('contingencyanalysismeasure',False) not in [False,'']:
		distance_type=parameters_user.get('contingencyanalysismeasure',False)
		if 'chi2' in distance_type:
			distance_type='chi2_dir'
		if 'deviation' in distance_type:
			distance_type='cooc_deviation'
		if 'cramer' in distance_type:
			distance_type="cramer_dir"
	print ('distance_type',distance_type)
	distance_thres=-9999999
	k_nearest=999999
	computeoptimaldistancethres=False
	
#print "distance_type",distance_type

try:
	top2 = int(parameters_user.get('top2',None))
	print ('thetop2',top2)
	top1_mult = float(top)/float(top2)
	top=top2
except:
	top2 = None
	top1_mult = 1.


print ('top',top)
print ('top2',top2)
print ('top1_mult',top1_mult)

overlap_size=parameters_user.get('overlap',False)
period_size=parameters_user.get('period_size',None)
recompute=parameters_user.get('recompute',True)
automatic_intertemporal_threshold=parameters_user.get('automatic_intertemporal_threshold',True)
geolocalized=parameters_user.get('geolocalized',False)
tagging=parameters_user.get('tagging',False)
robustness = parameters_user.get('robustness',False)
#dynamic_profile=parameters_user.get('dynamic_profile',False)
profile_dynamic_step=parameters_user.get('profile_dynamic_step',None)
try:
	if int(profile_dynamic_step)>=1:
		dynamic_profile=True
		profile_dynamic_step=int(profile_dynamic_step)
	else:
		dynamic_profile=True
		profile_dynamic_step=1
except:
	dynamic_profile=False
		

try:
	tagging_label_number=int(parameters_user.get('tagging_label_number',3))
except:
	tagging_label_number=3
tagging_dist=parameters_user.get('tagging_dist','tfidf')
tagging_mode=parameters_user.get('tagging_mode',False)


simplified_label=parameters_user.get('simplified_label',False)
arrows=parameters_user.get('arrows',False)
bdd_name = corpus_file

#print 'type_biparti param loading',type_biparti

#multiprocessing_activated=parameters_user.get('multiprocessing_activated',True)
#if nb_period==1:





#################################
###logging user parameters#######
#################################
logging.basicConfig(filename=os.path.join(result_path,'.user.log'), filemode='w', level=logging.DEBUG,format='%(asctime)s %(levelname)s : %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
logging.info('Starting script Profiling')

logging.info('Script Profiling Started')
yamlfile = 'profiling.yaml'
if 'cointet' in os.getcwd():
	yamlfile = '/Users/jpcointet/Desktop/cortext-methods/profiling/'+yamlfile
parameterslog=fonctions.getuserparameters(yamlfile,parameters_user)
logger = logging.getLogger()
logging.info(parameterslog)
logger.handlers[0].flush()
fonctions.progress(result_path0,4)
##################################
### end logging user parameters###
##################################
fonctions.check_bddname(corpus_file)
filename_v = fonctions.get_data(bdd_name,'ISIpubdate',limit_id=1)
try:
	filename=filename_v.values()[0][0]['file']
except:
	filename='f'


print ('bdd_name',bdd_name)


#from guppy import hpy
# h=hpy()
# print 'h.heap()',
# print h.heap()
#bdd_name_description = bdd_name[:-3]+'.yml'
dico_champs = {}

if corpus_type=='isi':
	dico_champs['Keywords'] = 'ISIkeyword'
	dico_champs['Cited Author'] = 'ISICRAuthor'
	dico_champs['Cited Journal'] = 'ISICRJourn'
	dico_champs['Cited References'] = 'ISICitedRef'
	dico_champs['References'] = 'ISIRef'
	dico_champs['Research Institutions'] = 'ISIC1Inst'
	dico_champs['Subject Category'] = 'ISISC'
	dico_champs['Countries'] = 'ISIC1Country'
	dico_champs['Journal'] = 'ISIJOURNAL'
	dico_champs['Journal (long)'] = 'ISISO'
	dico_champs['Cities'] = 'ISIC1City'
	dico_champs['Author'] = 'ISIAUTHOR'
	dico_champs['Terms'] = 'ISIterms'
	dico_champs['Cited References'] = 'ISICitedRef'
	dico_champs['cp'] = 'cp'
	dico_champs['Country'] = 'Country'
	dico_champs['Year'] = 'ISIpubdate'
	dico_champs['Periods'] = 'ISIpubdate_custom'
	dico_champs['Publication Type'] = 'ISIDT'
	dico_champs['WOS Category'] = 'ISIWC'
	dico_champs['Funding'] = 'ISIFU'

dico_champs['Terms']='ISIterms'
dico_champs['Periods'] = 'ISIpubdate_custom'
if corpus_type=='xmlpubmed' or corpus_type=='isi' or corpus_type=='ris' :
	dico_champs['Year'] = 'ISIpubdate'
else:
	dico_champs['Time Steps'] = 'ISIpubdate'
# print 'corpus_type',corpus_type
# print 'dico_champs',dico_champs
table1source=dico_champs.get(field1,field1)
table2source=dico_champs.get(field2,field2)


print ('table sources:',table1source,' & ',table2source)
#save parameters in method strings
method={}
method['nb_per'] = str(nb_period)[:8] + str(nb_period)[15:19] + str(nb_period)[25:30]
#print 'str(nb_period[:3])',str(nb_period)[:3]
method['field1'] = field1
method['field2'] = field2
method['time_sp'] = time_cut_type
method['het_typ'] = type_biparti
method['top_node']= top
method['period_size']=period_size
method['overl_size']=overlap_size
method['norm']=norm
method['computeoptimaldistancethres']=computeoptimaldistancethres
method_basic = deepcopy(method)#method basic: settings before reconstruction method is involved
parameters_basic = ';'.join(map(lambda x: str(x[0])+'-'+str(x[1]),method_basic.items()))

method['CDM']=community_detection_method
method['dist']=distance_type
method['distance_mode']=distance_mode
if 'k_nearest' in locals():
	method['k_nearest']=k_nearest
if 'top_edges' in locals():
	method['top_edges']=top_edges
if 'distance_thres' in locals():
	method['d_thres']=distance_thres

method_intermediate = deepcopy(method)#method basic: settings before reconstruction method is involved
parameters_intermediate = ';'.join(map(lambda x: str(x[0])+':'+str(x[1])[:10],method_intermediate.items()))


method['int-thres']=intertemporal_threshold
method['orph']=str(no_orphan)

parameters_advanced = ';'.join(map(lambda x: str(x[0])+':'+str(x[1]),method.items()))

method['no_orphan']=no_orphan

#print 'parameters:'
#print 'corpus_file',corpus_file
bdd_name=corpus_file

#print "ici : parameters_user.get('localcooc',False)",parameters_user.get('localcooc','tfidf')
#print 'parameters_user',parameters_user
#############################################################################
#############################################################################
##########################-EFFECTIVE CODE-###################################
#############################################################################
#############################################################################

#print 'type_biparti param loading',type_biparti

#connexion bdd
conn,curs=fonctions.create_bdd(bdd_name)
weight_table = conn.execute("select count(*) from sqlite_master where name=?",("weightattributes" ,)).fetchone()
democratic = parameters_user.get('democratic',False)
if weight_table[0]==1:
	weight_table=True
	#print 'weight_table',weight_table
	weight_dict = {}
	weight_list = conn.execute("select id,data from weightattributes").fetchall()
	for couple in weight_list:
		weight_dict[couple[0]]=float(couple[1])
else:
	if democratic:
		weight_table=True
		weight_dict={}
	else:
		weight_table=False
		weight_dict={}
	
#print 'weight_dict',weight_dict

weight_table = conn.execute("select count(*) from sqlite_master where name=?",(field1 + '_' + 'pos_x' ,)).fetchone()
if weight_table[0]==1:
	geolocalized=True

starttime= time.time()

#####################
#0. temporal slicing#
#####################
logging.info('Extracting Data')
fonctions.progress(result_path0,6)


years_bins_dict,result_pubdate,years_cover_dist = fonctions.cut_time(curs,nb_quantile=nb_period,cut_type=time_cut_type,period_size=period_size,overlap_size=overlap_size,time_table=time_table,sequence_type=parameters_user.get('sequence_type','snapshot'))
nb_period=len(years_bins_dict.keys())
years_bins_dict_inv=fonctions.inverse_dict_year(years_bins_dict)
#print 'years_bins_dict',str(years_bins_dict)[:1000],'...'
#print 'years_bins_dict_inv',str(years_bins_dict_inv)[:1000],'...'
years=list(map(lambda x:int(x.split('_')[1]),years_bins_dict.keys()))
#if parameters_user.get('sequence_type','snapshot')	=='growing':
#	years=map(lambda x:int(x.split('_')[1]),years_bins_dict.keys())
#print years_bins_dict
years.sort()
years_good=[]
for year in years:
	for y in years_bins_dict.keys():
		if y.split('_')[1]==str(year):
			years_good.append(y)
years=years_good
if parameters_user.get('sequence_type','snapshot')	=='growing':
	years=fonctions.reverse_list(years)
#print 'years_good',years

if len(years)==1:
	no_orphan=False#everyone is an orphan

years_mean = {}
#print 'years',years
year0 = int(years[0].split('_')[0])
yearf = int(years[-1].split('_')[1])-year0
for ye in years:
	years_mean[ye]=float(sum(map(lambda x: int(x),ye.split('_'))))/2. -year0
#print 'years_mean',str(years_mean)[:1000],'...'


years_bins_weight = {}
#print 'years_cover_dist',years_cover_dist
for years_bins in years_bins_dict:
	#print 'years_bins',years_bins
	#print 'years_bins_dict[years_bins]',years_bins_dict[years_bins]
	years_bins_weight[years_bins]=sum(map (lambda x:  years_cover_dist.get(x,0),years_bins_dict[years_bins]))
#print 'years_bins_weight',years_bins_weight
	
#####################
#1. build raw graphs#
#####################


if geolocalized:
	if 1:
		pos_field1 = get_position(field1)#,0.+float(random.randint(10))/10.)
		pos_field2 = get_position(field2)#,0.+float(random.randint(10))/10.)
		fonctions.afficher_dict(pos_field2,10)
		
	else:
		print ('WARNING !!!! unable to load geolocalized points')
		pass
	
	
print ('$$$$$$$$$$ result_pubdate built ',': ',(time.time()-starttime), ' seconds')
starttime= time.time()
#print 'type_biparti',type_biparti
def transform_2_occvect(results1):
	#print 'results1',results1
	if not rank==None:
		results1_no=fonctions.make_result_norank(results1)
	else:
		results1_no=results1
	#print 'results1_no',results1_no
	occurrences_dict =fonctions.inverse_dict_year(results1_no)
	return occurrences_dict
#print 'distance_mode',distance_mode



def load_data(table1source,time_table,years_bins_dict,top,rank,result_pubdate,years_bins_dict_inv,no_time=False,top1_mult=1):
	print ('loading data')
	sqlite_way=True
	if norm=='Time dependent':
		sqlite_way=False
	if sqlite_way:
		results1_y_top,list_item1_y={},{}
		fonctions.safe_execute(curs,'CREATE INDEX idx_data ON '+table1source+'(data)')
		fonctions.safe_execute(curs,'CREATE INDEX idx_data ON '+time_table+'(data)')
		for y in years_bins_dict.keys():			
			years_interval=years_bins_dict[y]
			sql_query=''
			if rank==None:
				sql_query += 'id,data'
			else:
				sql_query += 'id,rank,parserank,data'
			sql_query += ' FROM ' +table1source
			sql_query += ' WHERE data in ('
			sql_query+= 'SELECT data FROM ' + table1source
			if not no_time:
				if not parameters_user.get('freeze_nodes',False):
					sql_query+= ' WHERE id in '
					sql_query+='(SELECT id from '+time_table+' WHERE data<='+str(max(years_interval)) + ' and data>='+str(min(years_interval)) +')'
			sql_query += ' group by data order by count(id) desc limit ' +str(int(float(top)*top1_mult))+')'
			if not no_time:
				sql_query +=' and id in (SELECT id from '+time_table+' WHERE data<='+str(max(years_interval)) + ' and data>='+str(min(years_interval)) +')'
			sql_queryalpha = 'SELECT ' + sql_query
			sql_querybeta = 'SELECT '+'file,' + sql_query
			try:
				resultssqlite1 = curs.execute(sql_queryalpha).fetchall()
				print ('sql_queryalpha',sql_queryalpha)
			except:
				resultssqlite1 = curs.execute(sql_querybeta).fetchall()
				print ('sql_querybeta',sql_querybeta)
			try:
				if 'forbidenn_nodes' in parameters_user:
					results_dict,list_items=fonctions.turn_results_2_dict(resultssqlite1,rank,parameters_user.get('unicity',False),parameters_user.get('forbidenn_nodes',False),parameters_user.get('lowering',False))
				else:
					results_dict,list_items=fonctions.turn_results_2_dict(resultssqlite1,rank,parameters_user.get('unicity',False),parameters_user.get('lowering',False))
				results1_y_top[y]=results_dict
				list_item1_y[y]=list_items.keys()
			except:
				results1_y_top[y]={}
				list_item1_y[y]=[]
			#print ('list_item1_y',list_item1_y,results_dict)
			#print results1_y_top
	else:
		results1_y=fonctions.extract_data(table1source,result_pubdate,years_bins_dict,years_bins_dict_inv,curs,rank=rank,unicity=(parameters_user.get('unicity',False)))
		#print '$$$$$$$$$$ data extracted',': ',(time.time()-starttime), ' seconds'
		print( 'results1_y:')
		fonctions.afficher_dict(results1_y)
		#make unique results:
		print( 'topping results')
		results1_y_top,list_item1_y=fonctions.extract_top(results1_y,int(top*top1_mult),result_pubdate,norm=norm,rank=rank,forbidden_nodes=parameters_user.get('forbidenn_nodes',False))
		
	return results1_y_top,list_item1_y
#####################################
#2. Filtering heterogeneous networks#
#####################################
# try:
# 	if recompute:
# 		print 'automatically recompute'
# 		fonctions.dumpingout('skdlqmjmlaejfiojqsmfnqo'+parameters_basic,result_path)
# 	print "trying to load final_net_y and poids_y..."
# 	print parameters_intermediate
# 	final_net_y=fonctions.dumpingout('final_net_y'+parameters_intermediate,result_path)
# 	poids_y=fonctions.dumpingout('poids_y'+parameters_intermediate,result_path)
# 	print "loaded final_net_y and poids_y..."	
# 	list_item2_y=fonctions.dumpingout('list_item2_y'+parameters_basic,result_path)
# 	list_item1_y=fonctions.dumpingout('list_item1_y'+parameters_basic,result_path)
# 	results1_y=fonctions.dumpingout('results1_y'+parameters_basic,result_path)
# 	if table1source==table2source:
# 		results2_y=results1_y
# 	else:
# 		results2_y=fonctions.dumpingout('results2_y'+parameters_basic,result_path)
# 
# 	results1_y_top=fonctions.dumpingout('results1_y_top'+parameters_basic,result_path)
# 	results2_y_top=fonctions.dumpingout('results2_y_top'+parameters_basic,result_path)
# except:

if nb_period==1 and time_table == 'ISIpubdate':
	no_time=True
else:
	no_time=False
#no_time=False
#print 'no_time:', no_time
#if 1:	
	# try:#try to load list_item1_y, list_item2_y, and biparti_y from the database
	# 	
	# 	if recompute:
	# 		print 'automatically recompute'
	# 		fonctions.dumpingout('skdlqmjmlaejfiojqsmfnqo'+parameters_basic,result_path)
	# 		
	# 	if type_biparti == 'merged':
	# 		fail
	# 	print "trying to load biparti...",parameters_basic
	# 	####ATTENTION####!!!!
	# 
	# 	list_item2_y=fonctions.dumpingout('list_item2_y'+parameters_basic,result_path)
	# 	list_item1_y=fonctions.dumpingout('list_item1_y'+parameters_basic,result_path)
	# 
	# 	########COMMENT FOLLOWING FOR DEBUG
	# 	biparti_y=fonctions.dumpingout('biparti_y'+parameters_basic,result_path)
	# 	#biparti_y=fonctions.dumpingout('biparti_y'+parameters_basic,result_path)
	# 	results1_y=fonctions.dumpingout('results1_y'+parameters_basic,result_path)
	# 	results1_y_top=fonctions.dumpingout('results1_y_top'+parameters_basic,result_path)
	# 	if table1source==table2source:
	# 		results2_y=results1_y
	# 		results2_y_top=results1_y_top
	# 	else:
	# 		results2_y=fonctions.dumpingout('results2_y'+parameters_basic,result_path)
	# 		results2_y_top=fonctions.dumpingout('results2_y_top'+parameters_basic,result_path)
	# 	print "biparti data load succesful "	


	#except:#biparti matrix needs to be computed
local_cooc = parameters_user.get('localcooc',False)
bdd_path='/'.join(bdd_name.split('/')[:-1])
#print 'bdd_path',bdd_path

parameters_basic_pickle_dict = {}
parameters_basic_pickle_string = ''
parameters_basic_pickle_dict['bdd_name']=bdd_name.split('/')[-1]
parameters_basic_pickle_dict['table1source']=table1source
parameters_basic_pickle_dict['time_table']=time_table
parameters_basic_pickle_dict['nb_period']=nb_period
parameters_basic_pickle_dict['time_cut_type']=time_cut_type
parameters_basic_pickle_dict['period_size']=period_size
parameters_basic_pickle_dict['overlap_size']=overlap_size
parameters_basic_pickle_dict['top']=top
parameters_basic_pickle_dict['no_time']=no_time
parameters_basic_pickle_dict['table2source']=table2source
parameters_basic_pickle_dict['type_biparti']=type_biparti
parameters_basic_pickle_dict['localcooc']=local_cooc
parameters_basic_pickle_dict['computeoptimaldistancethres']=computeoptimaldistancethres
parameters_basic_pickle_dict['distance_type']=distance_type
parameters_basic_pickle_dict['distance_thres']=distance_thres
parameters_basic_pickle_dict['top_edges']=top_edges
parameters_basic_pickle_dict['k_nearest']=k_nearest
parameters_basic_pickle_dict['size_community_threshold']=size_community_threshold
parameters_basic_pickle_dict['distance_mode']=distance_mode

for x in parameters_basic_pickle_dict.keys():
	parameters_basic_pickle_string += x[:1]+'-'+str(parameters_basic_pickle_dict[x])[:25]
#print "parameters_basic_pickle_string",parameters_basic_pickle_string
#print 'parameters_basic',parameters_basic

rank=True



def turn_biparti_into_dataframe(biparti_y):
	df_y={}
	for period in biparti_y:
		biparti=biparti_y[period]
		#print ('biparti',biparti)
		lignes=[]
		colonnes=[]
		for couple in biparti:
			(c0,c1)=couple
			lignes.append(c0)
			colonnes.append(c1)
		lignes = list(set(lignes))
		colonnes = list(set(colonnes))
		columns=[]
		for c in colonnes:
			column=[]
			for l in lignes:
				#print ("c,l",c,l)
				column.append(biparti.get((l,c),0))
			columns.append(column)
		#print (lignes,colonnes,columns)		
		df=pd.DataFrame(columns, index=colonnes,columns=lignes)
		df_y[period]=df
	return df_y

#print 'type_biparti before all',type_biparti
try:
	################################################
	###### TRY TO RETRIEVE PAST COMPUTED DATA ######
	################################################
	if parameters_user.get('minimize_computation',False) and not 'projection_cluster' in field1 and not 'projection_cluster' in field2:
		print ('trying to retrieve parameters related to parameters ', parameters_basic_pickle_string)

		data_extracted=fonctions.dumpingout('data_extracted'+parameters_basic_pickle_string,bdd_path)
		final_net_y,poids_y,optimal_distance_thres_y={},{},{}
		for y,data in zip(years,data_extracted):
			final_net_y[y],poids_y[y],optimal_distance_thres_y[y]=data[0],data[1],data[2]
		results1_y_top=fonctions.dumpingout('results1_y_top'+parameters_basic_pickle_string,bdd_path)
		#print 'results1_y_top',results1_y_top
		results2_y_top=fonctions.dumpingout('results2_y_top'+parameters_basic_pickle_string,bdd_path)
		list_item1_y=fonctions.dumpingout('list_item1_y'+parameters_basic_pickle_string,bdd_path)
		list_item2_y=fonctions.dumpingout('list_item2_y'+parameters_basic_pickle_string,bdd_path)
	
		if robustness:
			data_extracted1=fonctions.dumpingout('data_extracted1'+parameters_basic_pickle_string,bdd_path)
			data_extracted2=fonctions.dumpingout('data_extracted2'+parameters_basic_pickle_string,bdd_path)
			for y,data in zip(years,data_extracted1):
				final_net1_y[y],poids1_y[y],optimal_distance_thres1_y[y]=data[0],data[1],data[2]
			for y,data in zip(years,data_extracted2):
				final_net2_y[y],poids2_y[y],optimal_distance_thres2_y[y]=data[0],data[1],data[2]
			results1_y_top=fonctions.dumpingout('results1_y_top'+parameters_basic_pickle_string,bdd_path)
			results2_y_top=fonctions.dumpingout('results2_y_top'+parameters_basic_pickle_string,bdd_path)
			list_item1_y=fonctions.dumpingout('list_item1_y'+parameters_basic_pickle_string,bdd_path)
			list_item2_y=fonctions.dumpingout('list_item2_y'+parameters_basic_pickle_string,bdd_path)
		print ('successfully loaded data')
	else:
		dqskjlskqsqdljk#stupid command
except:
			
	if 1:
		print ('new parameters!: build raw data...')
		print ('$$$$$$$$$$ data to be read',': ',(time.time()-starttime), ' seconds')
		starttime= time.time()
		#if table1source==table2source or tagging or projection:
		# 	rank=True
		# else:
		# 	rank=None
		# no that longer with rank = True and acutally the same result in the end
		#print 'rank',rank
		#print 'twin',twin

		#print '$$$$$$$$$$ data to be loaded from db first version ',': ',(time.time()-starttime), ' seconds'
		# starttime= time.time()
		# # extracting data version pédestre
		# results1_y=fonctions.extract_data(table1source,result_pubdate,years_bins_dict,years_bins_dict_inv,curs,rank=rank,unicity=(parameters_user.get('unicity',False)))
		# #print '$$$$$$$$$$ data extracted',': ',(time.time()-starttime), ' seconds'
		# print 'results1_y:'
		# fonctions.afficher_dict(results1_y)
		# #make unique results:
		# print 'topping results'
		# results1_y_top,list_item1_y=fonctions.extract_top(results1_y,int(top*top1_mult),result_pubdate,norm=norm,rank=rank)
		# print 'results1_y_top:'
		# fonctions.afficher_dict(results1_y_top,10)
		# print 'list_item1_y',list_item1_y
		# print '$$$$$$$$$$ data to be loaded from db first version ',': ',(time.time()-starttime), ' seconds'
		# starttime= time.time()

		## extracting data version sqlite
		#print years_bins_dict
		print ('$$$$$$$$$$ data to be loaded from db ',table1source,': ',(time.time()-starttime), ' seconds')
		starttime= time.time()
		print ('type_biparti before load',type_biparti)
		results1_y_top,list_item1_y=load_data(table1source,time_table,years_bins_dict,top,rank,result_pubdate,years_bins_dict_inv,no_time,top1_mult=top1_mult)
		#print 'list_item1_y',list_item1_y
		# machien = list_item1_y.values()[0]
		# machien.sort()
		# print 'list_item1_y','\n'.join(machien)
		print ('$$$$$$$$$$ data loaded from db ',table1source,': ',(time.time()-starttime), ' seconds')
		starttime= time.time()
		print ('results1_y_top:')
		fonctions.afficher_dict(results1_y_top,5)
		
		
		
		print ('$$$$$$$$$$ data topped',': ',(time.time()-starttime), ' seconds')
		starttime= time.time()
		logging.info('Computing Correlations')
		fonctions.progress(result_path0,8)
		
		fonctions.afficher_dict(list_item1_y,3)
		if table1source==table2source:
			#results2_y=results1_y
			results2_y_top=results1_y_top
			list_item2_y=list_item1_y
		else:
			# results2_y =fonctions.extract_data(table2source,result_pubdate,years_bins_dict,years_bins_dict_inv,curs,rank=rank,unicity=(parameters_user.get('unicity',False)))
			# results2_y_top,list_item2_y=fonctions.extract_top(results2_y,top,result_pubdate,norm=norm,rank=rank)
			# results2_y=0

			print ('$$$$$$$$$$ data to be loaded from db ',table2source,': ',(time.time()-starttime), ' seconds')
			starttime= time.time()
			results2_y_top,list_item2_y=load_data(table2source,time_table,years_bins_dict,top,rank,result_pubdate,years_bins_dict_inv,no_time)
			#print 'list_item2_y',len(list_item2_y['1975_2015']),list_item2_y['1975_2015']
			print ('$$$$$$$$$$ data loaded from db ',table2source,': ',(time.time()-starttime), ' seconds')
			starttime= time.time()
		print ('$$$$$$$$$$ data read',': ',(time.time()-starttime), ' seconds')
		starttime= time.time()
		
		
		################ICI CHECK
		print ('results2_y_top:')
		fonctions.afficher_dict(results2_y_top,5)
		#print results1_y_top.keys()
		print ('$$$$$$$$$$ data topped',': ',(time.time()-starttime), ' seconds')
		starttime= time.time()

		#save  list_item1_y, list_item2_y in the database
		if not recompute:
			if parameters_user.get('minimize_computation',False):
				fonctions.dumpingin(results1_y,'results1_y'+parameters_basic_pickle_string,bdd_path)
				fonctions.dumpingin(results2_y,'results2_y'+parameters_basic_pickle_string,bdd_path)

				fonctions.dumpingin(results1_y_top,'results1_y_top'+parameters_basic_pickle_string,bdd_path)
				fonctions.dumpingin(results2_y_top,'results2_y_top'+parameters_basic_pickle_string,bdd_path)

				fonctions.dumpingin(list_item1_y,'list_item1_y'+parameters_basic_pickle_string,bdd_path)
				fonctions.dumpingin(list_item2_y,'list_item2_y'+parameters_basic_pickle_string,bdd_path)



		print ('$$$$$$$$$$ data recorded',': ',(time.time()-starttime), ' seconds')
		starttime= time.time()

		#extract heterogeneous networks, type 1/type 2.
		#merge of the two tables if biparti=complex
		print ('type_biparti before merge',type_biparti)
		if type_biparti == True:
			results1_y_top_c = {}
			for y, results1_top in results1_y_top.iteritems():
				#print 'y',y
				results2_top = results2_y_top[y]
				merge12 = fonctions.merge(results1_top,results2_top,lambda x,y:x+y)
				results1_y_top[y]= merge12
				results2_y_top[y]= merge12


		print ('$$$$$$$$$$ biparti construction 1st part',': ',(time.time()-starttime), ' seconds')
		starttime= time.time()
		democratic=parameters_user.get('democratic',False)		
		
		if type_biparti == 'merged':
			biparti_y_1={}
			for y, results1_top in results1_y_top.iteritems():
				biparti={}
				for x in results1_top:
					biparti = fonctions.add_links_clique(biparti,results1_top[x],results1_top[x],False,rank=rank,weight_table=weight_dict.get(x,1),local_cooc=parameters_user.get('localcooc',False),localcoocrange=int(parameters_user.get('localcoocrange',5)),democratic=democratic,twin=twin,contextdecay=parameters_user.get('contextdecay',''))
				biparti_y_1[y] = biparti
			biparti_y_2={}
			for y, results2_top in results2_y_top.iteritems():
				biparti={}
				for x in results2_top:
						biparti = fonctions.add_links_clique(biparti,results2_top[x],results2_top[x],False,rank=rank,weight_table=weight_dict.get(x,1),local_cooc=parameters_user.get('localcooc',False),localcoocrange=int(parameters_user.get('localcoocrange',5)),democratic=democratic,twin=twin,contextdecay=parameters_user.get('contextdecay',''))
				biparti_y_2[y] = biparti
		else:
			biparti_y,biparti1_y,biparti2_y={},{},{}
			try:
				pool.close()
				pool.join()
			except:
				pass
			#pool_size = min(len(years),int(multiprocessing.cpu_count()))
			#if multiprocessing_activated:
			#	print 'launch pool size', pool_size
			#	pool = multiprocessing.Pool(processes=pool_size)
			#	data_extracted = pool.map(get_biparti, years)
			#else:
			data_extracted=[]
			yy=results1_y_top.keys()
			#memory_controller = sys.getsizeof(results1_y_top[yy[-1]]) * len(yy)
			#print ('memory_controller',memory_controller)
			#pool_size = min(len(years),int(multiprocessing.cpu_count()))
			#if multiprocessing_activated and float(memory_controller) / 1000000. < 10:
			#	print ('launch pool size', pool_size)
			#	pool = multiprocessing.Pool(processes=pool_size)
			#	data_extracted = pool.map(get_biparti, years)
			#else:
			for y in years:
				data_extracted.append(get_biparti(y,robustness))
			#try:
			#	pool.close()
			#	pool.join()
			#except:
			#	pass
				
			print ('$$$$$$$$$$ biparti construction last part',': ',(time.time()-starttime), ' seconds')
			starttime= time.time()
			
			for y,data in zip(years,data_extracted):
				biparti_y[y]=data[0]
				if robustness:
					biparti1_y[y]=data[1]
					biparti2_y[y]=data[2]
		
		
		
		if parameters_user.get('minimize_computation',False):
			fonctions.dumpingin(results1_y_top,'results1_y_top'+parameters_basic_pickle_string,bdd_path)
			fonctions.dumpingin(results2_y_top,'results2_y_top'+parameters_basic_pickle_string,bdd_path)

			fonctions.dumpingin(list_item1_y,'list_item1_y'+parameters_basic_pickle_string,bdd_path)
			fonctions.dumpingin(list_item2_y,'list_item2_y'+parameters_basic_pickle_string,bdd_path)

		if not recompute:
			pass
			#fonctions.dumpingin(biparti_y,'biparti_y'+parameters_basic,result_path)


	print ('$$$$$$$$$$ biparti built ',': ',(time.time()-starttime), ' seconds')
	starttime= time.time()
	
	
	
	print ('biparti',biparti_y)
	df_y=turn_biparti_into_dataframe(biparti_y)
	print ("df_y",df_y)
		#	row = 
		#	for c in colonnes:
	print ('$$$$$$$$$$ distances built ',': ',(time.time()-starttime), ' seconds')
	starttime= time.time()
	#if not recompute:
	#	fonctions.dumpingin(final_net_y,'final_net_y'+parameters_intermediate,result_path)
	#	fonctions.dumpingin(poids_y,'poids_y'+parameters_intermediate,result_path)


	
#fonctions.progress(result_path0,100)

def build_expected(df,mode='secteur'):
    brand_mentions_legend =df.sum(axis=1).index
    brand_mentions = df.sum(axis=1).values

    driver_mentions_legend = df.sum(axis=0).index
    driver_mentions = df.sum(axis=0).values
    
    N=driver_mentions.sum()
        #the expected number of mentions of each brand x driver cell given the total frequency of respective drivers and brand
    
    de = pd.DataFrame(np.matmul( np.transpose([np.array(brand_mentions)]),np.array([(driver_mentions)]))/N,index=brand_mentions_legend,columns=driver_mentions_legend)
    if mode=='secteur':
        pass
    elif mode == 'concurrence':
        for i in range(de.shape[0]):
            for j in range(de.shape[1]):
                de.iloc[i,j]=brand_mentions[i]*(driver_mentions[j]-df.iloc[i,j])/(N-brand_mentions[i])        
    return de

def build_upper_lower(df,statistical_model='binomial'):
	df_lower = df.copy()
	df_upper = df.copy()
	
	if statistical_model=='multinomial':
		for brand in list(df.index):
			vals=df.loc[brand].values
			try:
				res=statsmodels.stats.proportion.multinomial_proportions_confint(vals,alpha=0.05, method='sison-glaz')
				print ('sison-glaz')
			except:
				res=statsmodels.stats.proportion.multinomial_proportions_confint(vals,alpha=0.05, method='goodman')
				print ('goodman')

			df_lower.loc[brand]=list(map(lambda x: x[0]*sum(vals),res))
			df_upper.loc[brand]=list(map(lambda x: x[1]*sum(vals),res))
	else:
		brand_sum=df.sum(axis=1)
		driver_sum=df.sum(axis=0)
		N=driver_sum.sum()
		for b in df.index:
			for d in df.columns:
				target=df.loc[b][d]
				brand_total = brand_sum[b] - target
				(l,h)=statsmodels.stats.proportion.proportion_confint(target, brand_sum[b])
				df_lower.at[ b, d]=l*brand_sum[b]
				df_upper.at[b, d]=h*brand_sum[b]
	return df_upper,df_lower
	
def reorder_deviation(deviation,g):
    drivers = list(df.columns)
    brands = list(df.index)
    brands_new,drivers_new=[],[]
    for order in g.dendrogram_col.reordered_ind:
        drivers_new.append(drivers[order])
    for order in g.dendrogram_row.reordered_ind:
        brands_new.append(brands[order])

    return deviation.reindex(columns=drivers_new,index=brands_new)
    
def build_sim_growth(df,de,dd):
        deviation1=100*(df-de).div(de)
        deviation2=100*(df-de).div(dd)
        deviation1=deviation1[deviation1>0] 
        deviation1=deviation1.fillna(0)
        deviation2 = deviation2[deviation2<0]
        deviation2=deviation2.fillna(0)
        deviation=deviation2+deviation1
        return deviation

    
def build_sim_mutliply(df,de,dd):
        deviation1=df.div(de)
        deviation2=de.div(df)
        deviation1=np.log(deviation1[deviation1>1])
        deviation1=deviation1.fillna(0)
        deviation2 = -np.log(deviation2[deviation2>=1])
        deviation2=deviation2.fillna(0)
        deviation=deviation2+deviation1
        return deviation


def compute_deviation(df,df_lower=pd.DataFrame(),df_upper=pd.DataFrame(),method='croissance',mode='secteur'):
    de=build_expected(df,mode=mode)
    if method=='croissance':
        deviation=100*(df-de).div(de)
        try:
            deviation_upper=100*(df_upper-de).div(de)
            deviation_lower=100*(df_lower-de).div(de)

        except:
            deviation_upper=deviation
            deviation_lower=deviation
    if method=='croissance_sym':
        deviation = build_sim_growth(df,de,df)
        try:
            deviation_upper = build_sim_growth(df_upper,de,df)
            deviation_lower = build_sim_growth(df_lower,de,df)
        except:
            deviation_upper=deviation
            deviation_lower=deviation
        
    if method=='multiply_sym':
        deviation = build_sim_mutliply(df,de,df)
        #deviation=np.log(df.div(de))
        try:
            deviation_upper=build_sim_mutliply(df_upper,de,df)
            deviation_lower=build_sim_mutliply(df_lower,de,df)

        except:
            deviation_upper=deviation
            deviation_lower=deviation



    return deviation,deviation_lower,deviation_upper
	
	
def compute_deviation(df,df_lower=pd.DataFrame(),df_upper=pd.DataFrame(),method='croissance',mode='secteur'):
    if method=='odds_ratio':
        import statsmodels.api as sm
        mode='concurrence'
    de=build_expected(df,mode=mode)
        
    if method=='croissance':
        deviation=100*(df-de).div(de)
        try:
            deviation_upper=100*(df_upper-de).div(de)
            deviation_lower=100*(df_lower-de).div(de)

        except:
            deviation_upper=deviation
            deviation_lower=deviation
    if method=='croissance_sym':
        deviation = build_sim_growth(df,de,df)
        try:
            deviation_upper = build_sim_growth(df_upper,de,df)
            deviation_lower = build_sim_growth(df_lower,de,df)
        except:
            deviation_upper=deviation
            deviation_lower=deviation
        
    if method=='multiply_sym':
        deviation = build_sim_mutliply(df,de,df)
        #deviation=np.log(df.div(de))
        try:
            deviation_upper=build_sim_mutliply(df_upper,de,df)
            deviation_lower=build_sim_mutliply(df_lower,de,df)
        except:
            deviation_upper=deviation
            deviation_lower=deviation

    if method=='odds_ratio':
        brand_sum=df.sum(axis=1)
        driver_sum=df.sum(axis=0)
        N=driver_sum.sum()
        deviation,deviation_upper,deviation_lower=df.astype('float32').copy(),df.astype('float32').copy(),df.astype('float32').copy()        
        for b in df.index:
            for d in df.columns:
                target=df.loc[b][d]
                brand_total = brand_sum[b] - target
                driver_total = driver_sum[d] - target
                remaining = (N - brand_sum[b]) - (driver_total - target)
                ar=np.array([[target,brand_total],[driver_total,remaining]])
                


                #table = sm.stats.Table.from_data(ar)
                table=sm.stats.Table2x2(ar)


                logodd=table.log_oddsratio
                [logodd_minus,logodd_plus]=table.log_oddsratio_confint(.95)
                #print (logodd_minus,logodd_plus)
                deviation.at[ b, d]=logodd
                deviation_upper.at[ b, d]=logodd_plus
                deviation_lower.at[b, d]=logodd_minus
                
        #table.summary(method='normal')




    return deviation,deviation_lower,deviation_upper	
linscale=.5
def plot_deviation(deviation,forcemax=False):
    fig, ax = plt.subplots(1,1,figsize=(deviation.shape[1],deviation.shape[0]))
    #deviation=deviation*100
    if forcemax==False:
        devmin=deviation.min().min()
        devmax=deviation.max().max()
        maxmax=max(devmax,-devmin)
    else:
        devmin,devmax,maxmax=-forcemax,forcemax,forcemax
    #img = ax.imshow(deviation, norm=colors.SymLogNorm(linthresh=0.03, linscale=0.03,vmin=devmin, vmax=devmax),cmap='RdBu_r')
    #img = ax.imshow(deviation, norm=colors.SymLogNorm(linthresh=linthres, linscale=.5,vmin=-maxmax, vmax=maxmax),cmap=cmap)
    if 'sym' in method:
        devmin=-maxmax
        devmax=maxmax
    
    img = ax.imshow(deviation, norm=colors.SymLogNorm(linthresh=linthres, linscale=linscale,vmin=devmin, vmax=devmax),cmap=cmap)
    plt.xticks(rotation=90)


    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    #for axis in [ax.xaxis]:

    formatter = ScalarFormatter()
    formatter.set_scientific(False)
    
    cbar2=fig.colorbar(img,cax=cax,format=formatter, extend='both')
    #cbar2.set_major_formatter(formatter)

    # Loop over data dimensions and create text annotations.
    for i in range(len(deviation.index)):
        for j in range(len(deviation.columns)):
            val=deviation.iloc[i, j]
            if math.fabs(val)<linthres:
                text = ax.text(j, i, "{:.0f}".format(val),ha="center", va="center", color="black")
            else:
                text = ax.text(j, i, "{:.0f}".format(val),ha="center", va="center", color="w")
    
    #ax.clim((-maxmax, maxmax))
    ax.set_xticklabels(['']+list(deviation.columns))
    ax.set_yticks(range(len(deviation.index)))
    ax.set_yticklabels(list(deviation.index))
    locs= ax.get_yticks()
    #plt.show()

def rescale(v):
    if method=='odds_ratio':
        if v>0:
            return np.power(10,v)
        else:
            return 1/np.power(10,v)
    else:
        return v
		

    devmin=deviation[driver_mention0].min()
    devmax=deviation[driver_mention0].max()
    maxmax=max(devmax,-devmin)

    fig, ax = plt.subplots(figsize=(10,len(deviation[driver_mention0].index)/2))
    if type_ent=="driver":
        #col='tab:blue'
        #col='teal'
        col='steelblue'
        col2='cornflowerblue'
    else:
        col='tab:pink'
        col2='orchid'
    try:
        xerr=[-deviation_lower[driver_mention0].values+deviation[driver_mention0].values,deviation_upper[driver_mention0].values-deviation[driver_mention0].values]
        plt.barh(deviation[driver_mention0].index,deviation[driver_mention0].values,xerr=xerr,color=col,alpha=0.7, ecolor='silver', capsize=5)   


    except:
        plt.barh(deviation[driver_mention0].index,deviation[driver_mention0].values,color=col,alpha=0.7, ecolor='silver', capsize=5)   


    try:
        plt.barh(deviation_past[driver_mention0].index,deviation_past[driver_mention0].values,color=col2,alpha=0.3,height=.5) 
    except:
        pass
    print ('linthres',linthres)
    print("linscale",linscale)
    plt.xscale('symlog',linthreshx=linthres,linscalex=linscale,basex=baselog)
    if "multiply" in method:
        for i in range(1,int(linthres*100)+1):
            #if not int(math.fabs(i)) in [4]:
                plt.axvline(x=i/100,ls='--',lw=.5,c='grey')
                plt.axvline(x=-i/100,ls='--',lw=.5,c='grey')
        char_plus = 'x'
        char_minus = '÷'
        char_ont = ''


    else:
        char_plus = '+'
        char_minus = ''
        if 'sym' in method:
            char_minus = '/'
        char_ont = '%'
        if linthres<20:
            r=range(1,linthres+1)
        else:
            r=range(1,linthres+1,10)
        for i in r:
            #if not int(math.fabs(i)) in [4]:
                plt.axvline(x=i,ls='--',lw=.5,c='grey')
                plt.axvline(x=-i,ls='--',lw=.5,c='grey')

    for axis in [ax.xaxis]:
        formatter = ScalarFormatter()
        formatter.set_scientific(False)
        axis.set_major_formatter(formatter)
    if forcemax==None:
            if 'sym' in method:
                plt.xlim((-maxmax*2,maxmax*2))
            else:
                plt.xlim((devmin*2,devmax*2))
            forcemax=2*maxmax
    else:
        if 'sym' in method:
            plt.xlim((-forcemax,forcemax))
        else:
            plt.xlim((-forcemax,forcemax))
    plt.grid(True,which="minor",ls="-", color='0.65')
    font = {#'family': 'serif',
        'color':  'black',
        'weight': 'bold',
        'size': 8,
        }
    for i, v in enumerate(deviation[driver_mention0].values):
        #print ('deviation_past',deviation_past[driver_mention0].values[i])
        w=deviation_past[driver_mention0].values[i]
        if not w!=np.nan:
            
            if v<0:
                if v>w:
                    ax.text(max(v/3,-forcemax), i , char_minus+str("{:.1f}".format(v)) +char_ont+ str(" (+{:.1f})".format(v-w)), color='dimgray', va='center',ha='right',fontdict=font)
                else:
                    ax.text(max(v/3,-forcemax), i , char_minus+str("{:.1f}".format(v))+char_ont + str(" ({:.1f})".format(v-w)), color='dimgray', va='center',ha='right',fontdict=font)
            else:
                if v>w:
                    ax.text( min(v/3,forcemax), i , char_plus+str("{:.1f}".format(v))+char_ont+ str(" (+{:.1f})".format(v-w)), color='dimgray', va='center',ha='left',fontdict=font)#fontweight='bold',
                else:
                    ax.text( min(v/3,forcemax), i , char_plus+str("{:.1f}".format(v))+char_ont+ str(" ({:.1f})".format(v-w)), color='dimgray', va='center',ha='left',fontdict=font)#fontweight='bold',

            
            
        else:
            if v<0:
                #ax.text(v - 1, i , str("{:.1f}".format(v)), color='grey', va='center',ha='right')

                ax.text(max(v/3,-forcemax), i , char_minus+str("{:.1f}".format(v)+char_ont), color='dimgray', va='center',ha='right',fontdict=font)
            else:
                ax.text(min(v/3,forcemax), i , char_plus+str("{:.1f}".format(v)+char_ont), color='dimgray', va='center',ha='left',fontdict=font)#fontweight='bold',

                
                # including upper and lower limits
    #ax.errorbar(x, y + 1.5, xerr=xerr, yerr=yerr,lolims=lolims, uplims=uplims,marker='o', markersize=8,linestyle=ls)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    title=driver_mention0 + ' - salience ' 
    if mode == 'concurrence':
        title+= '(one vs the rest'
    if mode == 'secteur':
        title+= '(one vs all,'
    if 'sym' in method:
        title+= ' symetrized deviation rate)'
    else:
        title+= ' deviation rate)'
    plt.title(title )
    #plt.show()

def plot_bar_driver_h(driver_mention0,deviation,deviation_past=pd.DataFrame(),deviation_lower=pd.DataFrame(),deviation_upper=pd.DataFrame(),forcemax=None,type_ent='driver'):

    devmin=deviation[driver_mention0].min()
    devmax=deviation[driver_mention0].max()
    maxmax=max(devmax,-devmin)
    fm=maxmax
    fig, ax = plt.subplots(figsize=(10,len(deviation[driver_mention0].index)/2))
    if type_ent=="driver":
        #col='tab:blue'
        #col='teal'
        col='steelblue'
        col2='cornflowerblue'
    else:
        col='tab:pink'
        col2='orchid'
    try:
        xerr=[-deviation_lower[driver_mention0].values+deviation[driver_mention0].values,deviation_upper[driver_mention0].values-deviation[driver_mention0].values]
        plt.barh(deviation[driver_mention0].index,deviation[driver_mention0].values,xerr=xerr,color=col,alpha=0.7, ecolor='silver', capsize=5)   


    except:
        plt.barh(deviation[driver_mention0].index,deviation[driver_mention0].values,color=col,alpha=0.7, ecolor='silver', capsize=5)   


    try:
        plt.barh(deviation_past[driver_mention0].index,deviation_past[driver_mention0].values,color=col2,alpha=0.3,height=.5) 
    except:
        pass
    print ('linthres',linthres)
    print("linscale",linscale)
    if not method=='odds_ratio':
        plt.xscale('symlog',linthreshx=linthres,linscalex=linscale,basex=baselog)
    #else:
    #    deviation[deviation<linthres/100]
    if "multiply" in method:
        for i in range(1,int(linthres*100)+1):
            #if not int(math.fabs(i)) in [4]:
                plt.axvline(x=i/100,ls='--',lw=.5,c='grey')
                plt.axvline(x=-i/100,ls='--',lw=.5,c='grey')
        char_plus = 'x'
        char_minus = '÷'
        char_ont = ''


    else:
        char_plus = '+'
        char_minus = ''
        char_ont = '%'
        if 'sym' in method:
            char_minus = '/'
        if 'odds' in method:
            char_minus = '÷'
            char_plus = 'x'
            char_ont = ''



        if not "odds" in method:
            if linthres<20:
                r=range(1,linthres+1)
            else:
                r=range(1,linthres+1,10)
            for i in r:
            #if not int(math.fabs(i)) in [4]:
            
                plt.axvline(x=i,ls='--',lw=.5,c='grey')
                plt.axvline(x=-i,ls='--',lw=.5,c='grey')

    for axis in [ax.xaxis]:
        formatter = ScalarFormatter()
        formatter.set_scientific(False)
        axis.set_major_formatter(formatter)
    if forcemax==None:
            if 'sym' in method :
                plt.xlim((-maxmax*2,maxmax*2))
            elif 'odds' in method:
                plt.xlim((-maxmax*1.1,maxmax*1.1))
            else:
                plt.xlim((devmin*2,devmax*2))

    else:
        plt.xlim((-forcemax,forcemax))
    plt.grid(True,which="minor",ls="-", color='0.65')
    font = {#'family': 'serif',
        'color':  'black',
        'weight': 'bold',
        'size': 8,
        }
    for i, v in enumerate(deviation[driver_mention0].values):
		
        w=deviation_past[driver_mention0].values[i]
        #print ('deviation_pastw',w)
        if str(w)!='nan':
            if v<0:
                if v>w:
                    if 'odds' in method:
                        ax.text(max(-fm*.9,v/3), i , char_minus+str("{:.1f}".format(rescale(v))), color='dimgray', va='center',ha='right',fontdict=font)
                    else:
                        ax.text(max(-fm*.9,v/3), i , char_minus+str("{:.1f}".format(rescale(v))) +char_ont+ str(" (+{:.1f})".format(rescale(v)-rescale(w))), color='dimgray', va='center',ha='right',fontdict=font)
                else:
                    if 'odds' in method:
                            ax.text(max(-fm*.9,v/3), i , char_minus+str("{:.1f}".format(rescale(v))), color='dimgray', va='center',ha='right',fontdict=font)
                    else:
                        ax.text(max(-fm*.9,v/3), i , char_minus+str("{:.1f}".format(rescale(v)))+char_ont + str(" ({:.1f})".format(rescale(v)-rescale(w))), color='dimgray', va='center',ha='right',fontdict=font)
            else:
                if v>w:
                    if 'odds' in method:
                        ax.text( min(fm*.9,v/3), i , char_plus+str("{:.1f}".format(rescale(v))), color='dimgray', va='center',ha='left',fontdict=font)#fontweight='bold',
                    else:
                        ax.text(min(fm*.9,v/3), i , char_plus+str("{:.1f}".format(rescale(v)))+char_ont+ str(" (+{:.1f})".format(rescale(v)-rescale(w))), color='dimgray', va='center',ha='left',fontdict=font)#fontweight='bold',
                else:
                    if 'odds' in method:
                        ax.text( min(fm*.9,v/3), i , char_plus+str("{:.1f}".format(rescale(v))), color='dimgray', va='center',ha='left',fontdict=font)#fontweight='bold',
                    else:
                        ax.text( min(fm*.9,v/3), i , char_plus+str("{:.1f}".format(rescale(v)))+char_ont+ str(" ({:.1f})".format(rescale(v)-rescale(w))), color='dimgray', va='center',ha='left',fontdict=font)#fontweight='bold',

            
            
        else:
            if v<0:
                #ax.text(v - 1, i , str("{:.1f}".format(v)), color='grey', va='center',ha='right')
                ax.text(max(-fm*.9,v/3), i , char_minus+str("{:.1f}".format(rescale(v))+char_ont), color='dimgray', va='center',ha='right',fontdict=font)
            else:
                ax.text( min(fm*.9,v/3), i , char_plus+str("{:.1f}".format(rescale(v))+char_ont), color='dimgray', va='center',ha='left',fontdict=font)#fontweight='bold',
    xtickslocs = ax.get_xticks()
    print (xtickslocs)
    print(ax.get_xticks())
    labels=ax.get_xticks()
    ax.set_xticklabels(list(map (lambda x : "{:.1f}".format(rescale(x)),labels)))
	
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    for label in ax.get_xticklabels():
        label.set_rotation(90)
    
    title=driver_mention0 + ' - salience ' 
    if mode == 'concurrence':
        title+= '(one vs the rest'
    if mode == 'secteur':
        title+= '(one vs all,'
    if 'sym' in method:
        title+= ' symetrized deviation rate)'
    else:
        if 'odds' in method:
            title+= ' odds ratio)'
        else:
            title+= ' deviation rate)'
    title += str(y)
    plt.title(title )	
	
	
	
def plot_bar(item,deviation,deviation_past=pd.DataFrame(),deviation_lower=pd.DataFrame(),deviation_upper=pd.DataFrame(),forcemax=None):
    drivers = list(df.columns)
    brands = list(df.index)
    if item in drivers:
        plot_bar_driver_h(item,deviation,deviation_past,deviation_lower,deviation_upper,forcemax=forcemax)
    elif item in brands:
        plot_bar_driver_h(item,deviation.transpose(),deviation_past.transpose(),deviation_lower.transpose(),deviation_upper.transpose(),forcemax=forcemax,type_ent='brand')
    else:
        print("selected items is not in any list")
        print("Allowed drivers: "+', '.join(drivers))
        print("Allowed brands: "+', '.join(brands))
#plot_bar('Trending',deviation)







cmap='PuOr'
cmap='RdBu_r'
#method='croissance'
#method='multiply_sym'


mode=parameters_user.get('mode','secteur')
if mode=='one vs all':
	mode='secteur'
if mode=='one vs the rest':
	mode='concurrence'
#mode='concurrence'#"secteur"
#mode='secteur'


method=parameters_user.get('method','croissance_sym')
if method=='deviation rate':
	method = 'croissance_sym'
else:
	method = 'odds_ratio'

	
statistical_model=parameters_user.get('statistical_model','binomial')

fm=None
if parameters_user.get('scalemaxvalue',False):
	fm=float(parameters_user.get('scalemaxvalue_value',1))

print ('fm is',fm)
baselog=2
#method='croissance'
linescale=.2
nopast=False
try:
	linthres=int(parameters_user.get('lin_thres',8))
except:
	linthres=8


#print(df_y.keys())
h = 0
for y in years:
	
	df=df_y[y]
	print ('dfzero',df)
	df[df==0]=.1#random.random()/10.
	print("y,df.head()",y,df.head())
	df_upper,df_lower = build_upper_lower(df,statistical_model=statistical_model)
	#deviation = compute_deviation(df,method='croissance',mode='secteur')
	if nopast:
		deviation_past=pd.DataFrame()
	else:
		try:
			deviation_past=deviation.copy()
			print 
		except:
			deviation_past=pd.DataFrame()
	deviation,deviation_lower,deviation_upper = compute_deviation(df,df_lower,df_upper,method=method,mode=mode)#,df_lower,df_upper,method='croissance',mode=mode)
	devmin=deviation.min().min()
	devmax=deviation.max().max()
	maxmax=max(devmax,-devmin)
	#print (devmin,devmax)


	
	g=sns.clustermap(deviation,cmap=cmap,norm=colors.SymLogNorm(linthresh=linthres,linscale=.5,vmin=-maxmax, vmax=maxmax),annot=True,fmt='.0f')
	#plt.tight_layout()
	fig_filename = "clustermap_"+field1 + '_' + field2 + '_' + method + '_'+mode + '_'+statistical_model+ '_'+y
	plt.savefig(os.path.join(result_path,fig_filename+'.pdf'))
	plt.rcParams['svg.fonttype'] = 'none'
	plt.savefig(os.path.join(result_path,fig_filename+'.svg') , transparent = True, pad_inches=0, bbox_inches='tight')
	deviation=reorder_deviation(deviation,g)
	deviation_past=reorder_deviation(deviation_past,g)
	deviation_upper=reorder_deviation(deviation_upper,g)
	deviation_lower=reorder_deviation(deviation_lower,g)


	
	try:
		os.mkdir(os.path.join(result_path,'histograms'))
	except:
		pass
#plot_bar('AUDI',deviation)
	# for row in deviation.columns:
	#
	# 	print(row)
	# 	#plot_bar(row,deviation,pd.DataFrame(),deviation_upper,deviation_lower,forcemax=fm,)
	# 	try:
	# 		plot_bar(row,deviation,deviation_past,deviation_lower,deviation_upper,forcemax=fm)
	# 		plt.tight_layout()
	#
	#
	# 		plt.savefig(os.path.join(os.path.join(result_path,'histograms'),"histogram_"+field1 + '_'+ row + '_' + method + '_'+mode +y+'.pdf'))
	# 		plt.close()
	# 	except:
	# 		print ('row issue',row,deviation)
	# 		pass

	final = float(len(years) * len(list(deviation.index)))#*len(list(deviation.columns)))

	for row in deviation.index:
		
		if 1:
			h+=1
			print ('processing row ',row)
			print ('number',16+ float(h)/final * float(98-16))
			fonctions.progress(result_path0,int(16+ float(h)/final * float(98-16)))
			logging.info('processing row '+row)
			plot_bar(row,deviation,deviation_past,deviation_lower,deviation_upper,forcemax=fm)
			plt.tight_layout()
			import unidecode

			fname="histogram"+ '_' +unidecode.unidecode(row) + unidecode.unidecode(field1) + '_' + unidecode.unidecode(field2) +'_'+ method + '_'+mode + '_'+statistical_model+ '_'+y
			fname=fname[:199]
			plt.savefig(os.path.join(os.path.join(result_path,'histograms'),fname+'.pdf'))
			plt.rcParams['svg.fonttype'] = 'none'
			plt.savefig(os.path.join(os.path.join(result_path,'histograms'),fname+'.svg') , transparent = True, pad_inches=0, bbox_inches='tight')
			#plt.close()
		else:
			print ('index issue',row,deviation)
			pass

import zipfile
def zipdir(dirPath=None, zipFilePath=None, includeDirInZip=True):

    if not zipFilePath:
        zipFilePath = dirPath + ".zip"
    if not os.path.isdir(dirPath):
        raise OSError("dirPath argument must point to a directory. "
            "'%s' does not." % dirPath)
    parentDir, dirToZip = os.path.split(dirPath)
    #Little nested function to prepare the proper archive path
    def trimPath(path):
        archivePath = path.replace(parentDir, "", 1)
        if parentDir:
            archivePath = archivePath.replace(os.path.sep, "", 1)
        if not includeDirInZip:
            archivePath = archivePath.replace(dirToZip + os.path.sep, "", 1)
        return os.path.normcase(archivePath)

    outFile = zipfile.ZipFile(zipFilePath, "w",
        compression=zipfile.ZIP_DEFLATED)
    for (archiveDirPath, dirNames, fileNames) in os.walk(dirPath):
        for fileName in fileNames:
            filePath = os.path.join(archiveDirPath, fileName)
            outFile.write(filePath, trimPath(filePath))
        #Make sure we get empty directories as well
        if not fileNames and not dirNames:
            zipInfo = zipfile.ZipInfo(trimPath(archiveDirPath) + "/")
            outFile.writestr(zipInfo, "")
    outFile.close()
print ('result_path',result_path)
zipdir(os.path.join(result_path,'histograms'), os.path.join(result_path,'histograms.zip')) #Get a .zip file with a specific file name
# 	files = os.listdir(os.path.join(result_path,'cartos'))
# 	for file in files:
# 		shutil.copy(os.path.join(os.path.join(result_path,'cartos'),file),os.path.join(os.path.join(result_path_simple,'maps'),file))



#plot_f'AUDI',deviation)
#pt:
# 		pass
# 	try:
# 		os.mkdir(os.path.join(os.path.join(result_path_simple,'tubes'),'lib'))
# 	except:
# 		pass
#
# if clause_json_tube:
# 	shutil.copy(os.path.join(result_path,'phylo/energy.json'),os.path.join(os.path.join(os.path.join(result_path_simple,'tubes'),'data'),'energy.json'))
#
# if not 'Users/jean-philippecointet/' in os.getcwd() and not  'jpcointet/' in os.getcwd():
# 	print 'zipping ',result_path, 'into', os.path.join(result_path_simple,'maps_output.zip')
# 	zipdir(result_path, os.path.join(result_path_simple,'maps_output.zip')) #Get a .zip file with a specific file name
# 	files = os.listdir(os.path.join(result_path,'cartos'))
# 	for file in files:
# 		shutil.copy(os.path.join(os.path.join(result_path,'cartos'),file),os.path.join(os.path.join(result_path_simple,'maps'),file))
# 	files = os.listdir(os.path.join(os.path.join(result_path,'maps_export_format'),'gexf'))
# 	for file in files:
# 		print 'file',file
# 		#still need to prepare the gexf file and html for output...
# 		#shutil.copy(os.path.join(os.path.join(result_path,'cartos'),file),os.path.join(os.path.join(result_path_simple,'maps'),file))
# 	shutil.rmtree(os.path.join(result_path,'cartos'))
# 	shutil.rmtree(os.path.join(result_path,'maps_export_format'))
# 	shutil.rmtree(os.path.join(result_path,'cartohigh'))
# 	shutil.rmtree(os.path.join(result_path,'phylo'))
# 	shutil.move(os.path.join(result_path_simple,'maps_output.zip'),os.path.join(os.path.join(result_path_simple,'maps'),'maps_output.zip'))
# 	if clause_json_tube:
# 		files = os.listdir('sankey/')
# 		for file in files:
# 			shutil.copy(os.path.join('sankey/',file),os.path.join(os.path.join(os.path.join(result_path_simple,'tubes'),'lib'),file))
# 	if clause_json_tube:
# 		shutil.copy('sankey/index.htm',os.path.join(os.path.join(result_path_simple,'tubes'),'index.htm'))
#
#
#############################################################################
#############################################################################
##########################-CLI EXPORT-#######################################
#############################################################################
#############################################################################

fonctions.progress(result_path0,100)
#sys.path.append("../")
from librarypy import descriptor
descriptor.generate(bdd_name)
logger = logging.getLogger()

logging.info('Profiling script ended successfully')
logger.handlers[0].flush()

print ('$$$$$$$$$$ total elapsed time: ',': ',(time.time()-starttime0), ' seconds')

try:
	cli_path = "../../cli/"
	result_path_yaml_out=result_path
	(result_path_yaml_out,tail)= os.path.split(result_path)
	(result_path_yaml_out2,tail2)= os.path.split(result_path_yaml_out)
	yaml_dict={}
	yaml_dict['uri'] = tail2
	yaml_dict['description'] = 'Heterogeneous network'
	yaml_dict['structure'] = 'network'
	yaml_dict['type'] = 'json'
	yaml_dict['version']={}
	yaml_dict['version']['major']=int(tail)
	yaml_dict['version']['minor']=0
	import json
	cli_command = 'php ' + cli_path + "cortextcli.php dataset add "  + result_path_yaml_out
	cli_command = cli_command  + " '"+json.dumps(yaml_dict) + "'"
	print (cli_command)
	print (os.system(cli_command))
except:
	pass